""" 
@author: Darlin KUAJO
Ce module contient les fonctions utiles à l'implémentation de MI2IBRS.
"""
import os
import nltk
import string
import pandas as pd
from tqdm import tqdm
from math import sqrt, pow
from IPython.core.display import HTML

CSS = """
<style>
h1, th{
    text-align:center;
}
td{
    text-align:justify;
}
.dataframe{
    border-collapse:collapse;
    margin: 0 auto;
}
th, td{
    padding: 10px;
}
</style>
\n
"""

def getcategories(asin, metadata):
    """Cette fonction retoune la liste des catégories du produit identifié par asin
    
    asin : str
    metadata : pd.DataFrame
    
    Valeur de retour : list(str)
    """
    return metadata[metadata['asin'] == asin].reset_index(inplace=False, drop = True).iloc[0]['categories']

def getItemByIndex(index, metadata):
    """ Cette fonction permet de retourner l'identifiant d'un produit à partir de son index
    
    metadata : pd.DataFrame
    index : int
    
    index > 0
    
    Valeur de retour : str
    """
    return metadata.iloc[index]['asin']

def getUserByIndex(reviewerID, evaluation_matrix):
    """ Cette fonction permet de retourner l'identifiant d'un utilisateur à partir de son index
    
    evaluation_matrix : dict
    index : int
    
    index > 0
    
    Valeur de retour : str
    """
    users = list(evaluation_matrix.keys())
    return users[index]

def imUrl_to_image_html_width100(imUrl):
    """Cette fonction permet de convertir l'adressse url de l'image d'un produit en
    balise HTML nécessaire pour afficher une image cliquable avec une largeur de 100px
    
    imUrl : str
    
    Valeur de retour : str
    """
    
    return '<a href="' + imUrl + '">' + '<img src="'+ imUrl + '" width="100px" >' + '</a>'



def visualisation_select_product(k, asin, metadata):
    """ Cette fonction est dédiée à l'affichage du produit sélectionné.
    
    k : int
    asin : str
    metadata : pd.DataFrame
    
    k >= 0
    
    Valeur de retour : IPython.core.display.HTML, IPython.core.display.HTML
    """
    
    # Création du DataFrame nécessaire pour la visualisation du produit choisi
    print("[INFO] Génération d'un affichage du produit sélectionné ...")
    item_df = metadata[metadata['asin'] == asin]
    item_df.reset_index(inplace=True, drop=True)
    item_df = item_df.loc[:,['asin', 'description', 'categories', 'imUrl']]
    item_df.rename(columns={'imUrl' : 'image'}, inplace=True)
    code_html = item_df.to_html(escape=False,index=False, formatters=dict(image=imUrl_to_image_html_width100))
    
    # Création d'un fichier .html à la racine du projet contenant une visualisation du produit sélectionné et la liste de recommandation
    print("[INFO] Création du fichier de visualisation ...")
    with open("top-" + str(k) + "-recommandation.html", 'w', encoding='UTF-8') as fichier:
        fichier.write(CSS)
        fichier.write(code_html.replace("""<table border="1" class="dataframe">""", 
                                        """ <table border="1" class="dataframe"> \n <caption><h1>Produit sélectionné</h1></caption>"""))
        fichier.write("\n")
        
    return HTML(item_df.to_html(escape=False, formatters=dict(image=imUrl_to_image_html_width100)))

def visualisation_recommended_products(k, recommendations_list, score_name, list_title, metadata):
    """ Cette fonction est dédiée à l'affichage du produit sélectionné et de la liste de recommandation
    
    k : int
    recommendations_list : list(flozt, str)
    name_score : str
    metadata : pd.DataFrame
    
    k >= 0
    
    Valeur de retour : IPython.core.display.HTML, IPython.core.display.HTML
    """
    
    # Création du DataFrame qui présentera la liste de recommandation
    print("[INFO] Génération d'un affichage de la liste de recommandation ...")
    score = []
    item = []
    for i in range(len(recommendations_list)):
        score.append(recommendations_list[i][0])
        item.append(recommendations_list[i][1])
    recommended_products = pd.DataFrame({"asin" : item, score_name : score})
    recommended_products = pd.merge(recommended_products, metadata, how='inner', on='asin')
    recommended_products = recommended_products.rename(columns={'imUrl':'image'})
    recommended_products = recommended_products.loc[:, ['asin', score_name, 'description', 'categories','image']]
    code_html = recommended_products.to_html(escape=False, formatters=dict(image=imUrl_to_image_html_width100))
    
    # Création d'un fichier .html à la racine du projet contenant une visualisation du produit sélectionné et la liste de recommandation
    print("[INFO] Mise à jour du fichier de visualisation ...")
    with open("top-" + str(k) + "-recommandation.html", 'a', encoding='UTF-8') as fichier:
        fichier.write(code_html.replace("""<table border="1" class="dataframe">""", 
                                        """ <table border="1" class="dataframe"> \n <caption><h1>""" + list_title + """</h1></caption>"""))
        fichier.write("\n")
        
    return HTML(recommended_products.to_html(escape=False, formatters=dict(image=imUrl_to_image_html_width100)))

def transform_MUI_to_MIU(evaluation_matrix_UI):
    """ Cette fonction permet de transformer une matrice Users/Items en matrice Items/Users.
    
    evaluation_matrix_UI : dict
    
    Valeur de retour : dict
    """
    evaluation_matrix_IU = {}
    for user in evaluation_matrix_UI:
        for item in evaluation_matrix_UI[user]:
            evaluation_matrix_IU.setdefault(item, {})
            evaluation_matrix_IU[item][user] = evaluation_matrix_UI[user][item]
    
    return evaluation_matrix_IU

def sim_euclidean(evaluation_matrix, item1, item2):
    """Cette fonction renvoie la similarité euclidienne normalisée pour les produits item1 et item2
    
    evaluation_matrix : dict
    item1 : str
    item2 : str
    
    Valeur de retour : float
    """
    
    # Obtenir la liste des utilisateurs communs
    similars_users={}
    for user in evaluation_matrix[item1]:
        if user in evaluation_matrix[item2]:
            similars_users[user] = 1
    # s'ils n'ont aucun utilisateur en commun, retournez 0
    if len(similars_users) == 0:
        return 0
    # Additionnez les carrés de toutes les différences
    sum_of_squares=sum([pow(evaluation_matrix[item1][user] - evaluation_matrix[item2][user],2) 
                        for user in similars_users])

    return 1/(1+(sqrt(sum_of_squares)))

def top_k_recommendation(user, asin, k, evaluation_matrix, metadata, similarity_mesure):
    """Cette fonction renvoie la liste des k premiers produits pouvant être recommandés à user,
    selon l'ordre décroissant des scores de similarité euclidienne nomalisées par rapport au
    produit identifié par asin et leur appartenance à la même catégorie que le produit identifié par asin 
    
    La taille de la liste renvoyée est <= k
    evaluation_matrix est au format items/users
    
    user : str
    asin : str
    k : int
    evaluation_matrix : dict
    metadata : pd.DataFrame
   similarity_mesure : function
    
    k > 0
        
     Valeur de retour : list(IPython.core.display.HTML, IPython.core.display.HTML)
    """
    categories = getcategories(asin, metadata)
    # Détermination des scores de similarité entre le produits identifié par asin et tous les autres produits appatenant à la même catégories
    print("[INFO] Calcul des scores de similarités ...")
    
    recommendations_list = [(similarity_mesure(evaluation_matrix, asin, other_item), other_item) \
                            for other_item in evaluation_matrix if other_item != asin \
                           ]
    #Conservons uniquement ceux dont le score de similarité est >= 0.5
    recommendations_list = [(similarity_score, item) for similarity_score , item in recommendations_list if similarity_score >= 0.5]
    # Trions cette liste par ordre décroissant des scores de similarité
    print("[INFO] Tri de la liste par ordre décroissaant des scores de similarité ...")
    recommendations_list.sort(reverse=True)
     #Extrayons de cette liste tous les produits ayant la meme categorie que le produit sélectionnés
    print("[INFO] Filtrage de la liste en fonction de la cotégorie du produit choisi ...")
    same_product_categories = []
    for score_similarity, item in recommendations_list:
        if len(same_product_categories) != k:
            if categories == getcategories(item, metadata):
                same_product_categories.append((score_similarity, item)) 
    recommendations_list = same_product_categories
    
    return (visualisation_select_product(k, asin, metadata),
            visualisation_recommended_products(k, recommendations_list, "euclidean_similarity", "Les clients ont également appréciés", metadata)
           )